-- Scripts para o PostgreSQL

-- Tabela semáforo
CREATE TABLE public.trafficlight
(
  trafficlightid integer NOT NULL,
  status integer,
  timered integer,
  timeyellow integer,
  timegreen integer,
  personiscrossing boolean,
  lastupdate timestamp without time zone,
  CONSTRAINT trafficlight_pkey PRIMARY KEY (trafficlightid)
);

-- Log semáforo
CREATE TABLE public.trafficlightlog
(
  trafficlightfk integer,
  status integer,
  updated timestamp without time zone
);

ALTER TABLE public.trafficlight ADD COLUMN latitude float
ALTER TABLE public.trafficlight ADD COLUMN longitude float

-- Tabela localização
CREATE TABLE public.location
(
  locationid integer NOT NULL,
  locationname varchar(255),
  latitude decimal,
  longitude decimal,
  CONSTRAINT location_pkey PRIMARY KEY (locationid)
);

-- Tabela tipo localização
CREATE TABLE locationtype
(
	locationtypeid integer NOT NULL,
	locationtypename varchar(255),
	CONSTRAINT locationtype_pkey PRIMARY KEY (locationtypeid)
);

-- Tabela relação localização e tipo
CREATE TABLE relationlocationtype
(
	locationtypeid integer NOT NULL,
	locationid integer NOT NULL,
);

ALTER TABLE relationlocationtype  WITH CHECK ADD CONSTRAINT location_locationtype_locationtypeid_FK FOREIGN KEY(locationtypeid)
REFERENCES locationtype (locationtypeid)

ALTER TABLE relationlocationtype  WITH CHECK ADD CONSTRAINT location_locationtype_locationid_FK FOREIGN KEY(locationid)
REFERENCES location (locationid)

-- Dados Teste
INSERT INTO public.location VALUES(1,'bloco 1b ufu',-18.91867,-48.25967);
INSERT INTO public.location VALUES(2,'estação 6 ufu joão naves',-18.91799,-48.26074);
INSERT INTO public.location VALUES(3,'bloco 5ra ufu',-18.91796,-48.25910);
INSERT INTO public.location VALUES(4,'bloco 5rb ufu',-18.91769,-48.25910);
INSERT INTO public.location VALUES(5,'bloco 1a ufu atendimento ao aluno',-18.91816,-48.25839);
INSERT INTO public.location VALUES(6,'biblioteca ufu',-18.91706,-48.25853);
INSERT INTO public.location VALUES(7,'estação 1 ufu segismundo pereira',-18.91677,-48.25523);

INSERT INTO locationtype VALUES(1, 'biblioteca');
INSERT INTO locationtype VALUES(2, 'ponto de ônibus');
INSERT INTO locationtype VALUES(3, 'bloco estudo');

INSERT INTO relationlocationtype VALUES(3,1);
INSERT INTO relationlocationtype VALUES(2,2);
INSERT INTO relationlocationtype VALUES(3,3);
INSERT INTO relationlocationtype VALUES(3,4);
INSERT INTO relationlocationtype VALUES(3,5);
INSERT INTO relationlocationtype VALUES(1,6);
INSERT INTO relationlocationtype VALUES(2,7);

SELECT loc.*, typ.locationtypename
FROM location loc
	INNER JOIN relationlocationtype rlt ON loc.locationid = rlt.locationid
	INNER JOIN locationtype typ ON rlt.locationtypeid = typ.locationtypeid 
WHERE typ.locationtypename like '%ônibus%'
